package mypackage.iplproject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");

        List<String> queries = getQueries();

        findNumberOfMatchesPlayedPerYear(queries);
        findNumberOfMatchesWonByTeamsOverAllYears(queries);
        findNumberOfExtraRunsConcededByTeamIn2016(queries);
        findMostEconomicalBowlerIn2015(queries);
        findNumberOfMatchesWonByTeamInHomeGround(queries);


    }


    private static void findNumberOfMatchesPlayedPerYear(List<String> queries) throws ClassNotFoundException, SQLException {

        String query = queries.get(0);
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ProjectData", "sammy", "password");
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        System.out.println("No. of matches played per year over all years : ");
        while (rs.next()) {
            System.out.println(rs.getInt(1) + " - " + rs.getInt(2));
        }
        con.close();
    }

    private static void findNumberOfMatchesWonByTeamsOverAllYears(List<String> queries) throws SQLException, ClassNotFoundException {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ProjectData", "sammy", "password");
        Statement stmt = con.createStatement();
        String query = queries.get(1);
        ResultSet rs = stmt.executeQuery(query);

        System.out.println();
        System.out.println("Number of matches won by each team over all years : ");
        while (rs.next()) {
            System.out.println(rs.getString(1) + " - " + rs.getInt(2));
        }
        con.close();
    }


    private static void findNumberOfExtraRunsConcededByTeamIn2016(List<String> queries) throws ClassNotFoundException, SQLException {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ProjectData", "sammy", "password");
        Statement stmt = con.createStatement();
        String query = queries.get(2);
        ResultSet rs = stmt.executeQuery(query);

        System.out.println();
        System.out.println("Extra Runs Conceded by each team in 2016 :");
        while (rs.next()) {
            System.out.println(rs.getString(1) + " - " + rs.getInt(2));
        }
        con.close();
    }

    private static void findMostEconomicalBowlerIn2015(List<String> queries) throws ClassNotFoundException, SQLException {
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ProjectData", "sammy", "password");
        Statement stmt = con.createStatement();
        String query =queries.get(3);
        ResultSet rs = stmt.executeQuery(query);

        System.out.println();
        System.out.print("Most Economical Bowler of 2015 is : ");
        rs.next();
        System.out.println(rs.getString(1));
        con.close();
    }

    private static void findNumberOfMatchesWonByTeamInHomeGround(List<String> queries) throws ClassNotFoundException, SQLException {
        //Team I have chosen is SunRisers Hyderabad and their homeground is Hyderabad
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ProjectData", "sammy", "password");
        Statement stmt = con.createStatement();
        String query = queries.get(4);
        ResultSet rs = stmt.executeQuery(query);

        System.out.println();
        System.out.print("No. of Matches Won by Sunrisers Hyderabad in Hyderabad : ");
        rs.next();
        System.out.println(rs.getInt(2));
        con.close();
    }


    private static List<String> getQueries() throws IOException {
        List<String> queries = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader("./Queries.sql"));
        String line = "";
        while ((line = br.readLine()) != null) {
            queries.add(line);
        }
        return queries;
    }
}
